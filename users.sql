CREATE EXTENSION pgcrypto;

CREATE TABLE IF NOT EXISTS users (
    id SERIAL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    email TEXT NOT NULL,
    password_hash TEXT NOT NULL,
	position_id INT NOT NULL,
    PRIMARY KEY (id),
	FOREIGN KEY (position_id) REFERENCES positions(id),
    CONSTRAINT uniq_email UNIQUE (email)
);


CREATE TABLE IF NOT EXISTS positions (
    id SERIAL,
    position_name TEXT NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS permissions (
    id SERIAL,
    position_id INT NOT NULL,
    permission_name TEXT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (position_id) REFERENCES positions(id)
);

CREATE TABLE IF NOT EXISTS reset_password (
	id SERIAL,
    user_id INT NOT NULL,
	code_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	code TEXT NOT NULL,
    PRIMARY KEY (id),
	FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT uniq_code UNIQUE (code)
);

CREATE OR REPLACE FUNCTION func_hash_pass() RETURNS TRIGGER AS $$
BEGIN
  new.password_hash = crypt(new.password_hash, gen_salt('bf', 5));
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_hash_pass BEFORE INSERT ON users FOR EACH ROW EXECUTE PROCEDURE func_hash_pass();

CREATE OR REPLACE FUNCTION func_update_user() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.first_name IS NULL THEN
  	NEW.first_name = OLD.first_name;
  END IF;
  IF NEW.last_name IS NULL THEN
  	NEW.last_name = OLD.last_name;
  END IF;
  IF NEW.email IS NULL THEN
  	NEW.email = OLD.email;
  END IF;
  IF NEW.password_hash IS NULL THEN
  	NEW.password_hash = OLD.password_hash;
  ELSIF new.password_hash IS NOT NULL AND new.password_hash != (SELECT password_hash FROM users where id=new.id) THEN
        new.password_hash = crypt(new.password_hash, gen_salt('bf', 5));
  END IF;
  IF NEW.position_id IS NULL THEN
  	NEW.position_id = OLD.position_id;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_user BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE func_update_user();

CREATE OR REPLACE FUNCTION delete_old_codes() RETURNS TRIGGER AS $$
BEGIN
   IF (SELECT exists (SELECT 1 FROM reset_password WHERE user_id = NEW.user_id LIMIT 1)) = TRUE THEN
	DELETE FROM reset_password WHERE user_id=NEW.user_id;
   END IF;
   RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_delete_old_codes BEFORE INSERT ON reset_password FOR EACH ROW EXECUTE PROCEDURE delete_old_codes();

/*
SELECT cron.schedule('0 3 * * *', $$DELETE FROM reset_password WHERE code_time < CURRENT_TIMESTAMP - INTERVAL '23 hours'$$);

Cron potrzebuje być doinstalowany ręcznie, jednak nie jest on konieczny do uruchomienia i dzialania aplikacji. Jest to jedna z dodatkowych funkcjonalności.
https://github.com/citusdata/pg_cron <- Tutaj poradnik jak poprawnie zainstalować i skonfigurować rozszerzenie
*/
