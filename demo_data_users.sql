INSERT INTO positions VALUES(DEFAULT, 'root');

INSERT INTO permissions VALUES(DEFAULT, 1, 'add_position');
INSERT INTO permissions VALUES(DEFAULT, 1, 'delete_user');
INSERT INTO permissions VALUES(DEFAULT, 1, 'edit_order');
INSERT INTO permissions VALUES(DEFAULT, 1, 'manage_features');
INSERT INTO permissions VALUES(DEFAULT, 1, 'add_order');
INSERT INTO permissions VALUES(DEFAULT, 1, 'add_package');
INSERT INTO permissions VALUES(DEFAULT, 1, 'delete_client');
INSERT INTO permissions VALUES(DEFAULT, 1, 'manage_package');
INSERT INTO permissions VALUES(DEFAULT, 1, 'update_user');
INSERT INTO permissions VALUES(DEFAULT, 1, 'update_position');
INSERT INTO permissions VALUES(DEFAULT, 1, 'update_client');
INSERT INTO permissions VALUES(DEFAULT, 1, 'search_order');
INSERT INTO permissions VALUES(DEFAULT, 1, 'add_client');
INSERT INTO permissions VALUES(DEFAULT, 1, 'add_user');
INSERT INTO permissions VALUES(DEFAULT, 1, 'manage_discounts');
INSERT INTO permissions VALUES(DEFAULT, 1, 'delete_position');
INSERT INTO permissions VALUES(DEFAULT, 1, 'delete_package');

INSERT INTO users VALUES(DEFAULT, 'Grzegorz', 'Gajewski', 'pachoocha.wro@onet.pl', 'Bezimek1', 1);