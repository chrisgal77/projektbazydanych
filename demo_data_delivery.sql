INSERT INTO capacity_multipliers VALUES(DEFAULT, 0, 6000);
INSERT INTO capacity_multipliers VALUES(DEFAULT, 30000, 5000);
INSERT INTO capacity_multipliers VALUES(DEFAULT, 100000, 4000);

INSERT INTO weight_multipliers VALUES(DEFAULT, 0, 70.3240);
INSERT INTO weight_multipliers VALUES(DEFAULT, 10, 68.1240);
INSERT INTO weight_multipliers VALUES(DEFAULT, 20, 66.4240);
INSERT INTO weight_multipliers VALUES(DEFAULT, 30, 64.3270);
INSERT INTO weight_multipliers VALUES(DEFAULT, 40, 62.4240);
INSERT INTO weight_multipliers VALUES(DEFAULT, 50, 60.7240);

INSERT INTO states VALUES(DEFAULT, 'posted');
INSERT INTO states VALUES(DEFAULT, 'delivered');
INSERT INTO states VALUES(DEFAULT, 'in delivery');

INSERT INTO addresses (house_number, road, city, country) VALUES('22', 'Amarantowa', 'Wrocław', 'Polska');
INSERT INTO addresses (house_number, road, city, country) VALUES('32', 'wybrzeże Stanisława Wyspiańskiego', 'Wrocław', 'Polska');
INSERT INTO addresses (house_number, road, city, country) VALUES('3', 'Kwiatowa', 'Gniezno', 'Polska');

INSERT INTO stations VALUES(DEFAULT, 'Baza główna Paczoocha', 1);
INSERT INTO stations VALUES(DEFAULT, 'Sortownia PWR', 2);
INSERT INTO stations VALUES(DEFAULT, 'Sortownia Pod Grunwaldem', 3);

INSERT INTO features_list VALUES(DEFAULT, 'Carefully', 10.9970);
INSERT INTO features_list VALUES(DEFAULT, 'Radiopasive', 5.2115);
INSERT INTO features_list VALUES(DEFAULT, 'Insurance', 30.2317);

INSERT INTO discounts_list VALUES(DEFAULT, 'NowFamily', 5.930);
INSERT INTO discounts_list VALUES(DEFAULT, 'BlackFriday', 2.2115);
INSERT INTO discounts_list VALUES(DEFAULT, 'Xmas', 1.2345);