from dotenv import find_dotenv, load_dotenv


def load_local_env_vars():
    load_dotenv(find_dotenv())
