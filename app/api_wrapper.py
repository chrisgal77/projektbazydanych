import requests


class NominatimAPI:
    def __init__(self):
        self.api_url = NOMINATIM_API_URL = "https://nominatim.openstreetmap.org"
        self.search_endpoint = f"{self.api_url}/search"
        self.details_endpoint = f"{NOMINATIM_API_URL}/details"
        self.reverse_endpoint = f"{NOMINATIM_API_URL}/reverse"

        self.query_params = {"addressdetails": 1}

    def search(self, address_info):

        params_query = "&".join(
            f"{param_name}={param_value}"
            for param_name, param_value in self.query_params.items()
        )
        address_info = "+".join([str(info) for info in address_info.values()])
        request_url = (
            f"{self.search_endpoint}?q={address_info}&{params_query}&format=json"
        )

        response = requests.get(request_url)
        response.raise_for_status()
        json_response = response.json()

        if json_response == []:
            return None

        return json_response[0]["address"]

print(NominatimAPI().search({"city": "Gniezno Kwiatowa 3"}))
