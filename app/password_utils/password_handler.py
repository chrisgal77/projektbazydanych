import os
import os.path
import random
import smtplib
import string
from email.mime.text import MIMEText


class EmailSender:
    def __init__(self):
        self.port = "465"
        self.smtp_server = "smtp.poczta.onet.pl"

    def send_email(self, user_email, link):
        message = f"""

        Your credentials:
        - username: {user_email}
        
        Change your password at: 
        http://pachoocha.ddns.net/reset_password/{link}

        Best regards,
        Pachoocha Team
        """
        msg = MIMEText(message)

        msg["Subject"] = "You've just created an account!"

        server = smtplib.SMTP_SSL(f"{self.smtp_server}:{self.port}")

        server.login(os.environ.get("EMAIL"), os.environ.get("EMAIL_PASSWORD"))
        server.sendmail(os.environ.get("EMAIL"), user_email, msg.as_string())
        server.quit()
        print(f"Email sent to {user_email}")

    def send_password_reset(self, user_email, link):
        message = f"""

        Change your password at: 
         http://pachoocha.ddns.net/reset_password/{link}

        Best regards,
        Pachoocha Team
        """
        msg = MIMEText(message)

        msg["Subject"] = "Reset password!"

        server = smtplib.SMTP_SSL(f"{self.smtp_server}:{self.port}")

        server.login(os.environ.get("EMAIL"), os.environ.get("EMAIL_PASSWORD"))
        server.sendmail(os.environ.get("EMAIL"), user_email, msg.as_string())
        server.quit()
        print(f"Email sent to {user_email}")

    def send_order_number(self, user_email, order_id):
        message = f"""

        Your order is available at: 
        http://pachoocha.ddns.net/order/show/{order_id}

        Best regards,
        Pachoocha Team
        """
        msg = MIMEText(message)

        msg["Subject"] = "New order at Pachoocha!"

        server = smtplib.SMTP_SSL(f"{self.smtp_server}:{self.port}")

        server.login(os.environ.get("EMAIL"), os.environ.get("EMAIL_PASSWORD"))
        server.sendmail(os.environ.get("EMAIL"), user_email, msg.as_string())
        server.quit()
        print(f"Email sent to {user_email}")


class PasswordGenerator:
    def __init__(self):
        self.password_length = int(os.environ.get("PASSWORD_LENGTH"))
        self.link_length = int(os.environ.get("LINK_LENGTH"))

    def generate_password(self):
        pool = string.ascii_letters + str(string.digits) + "!" + "@" + "#" + "$" + "%"
        password = "".join(random.choice(pool) for i in range(self.password_length))
        return password

    def generate_link(self):
        pool = string.ascii_letters + str(string.digits)
        link = "".join(random.choice(pool) for i in range(self.link_length))
        return link
