import logging
import os
import re
from functools import wraps

from api_wrapper import NominatimAPI
from database import (
    ClientInput,
    DeliveryDatabaseManager,
    PackageInput,
    PositionInput,
    UserDatabaseManager,
    UserInput,
    permissions_mapping,
)
from flask import Flask, flash, redirect, render_template, request, session
from flask.helpers import url_for
from utils import load_local_env_vars

log_formatter = logging.Formatter(
    fmt="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)

logger = logging.getLogger("app")
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler("app.log", "a", "utf-8")
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(log_formatter)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.WARNING)

console_handler.setFormatter(log_formatter)

logger.addHandler(console_handler)
logger.addHandler(file_handler)


load_local_env_vars()

delivery_db = DeliveryDatabaseManager(
    dbname=os.environ.get("DELIVERYDB_NAME"),
    user=os.environ.get("DELIVERYDB_USER"),
    password=os.environ.get("DELIVERYDB_PASSWORD"),
    host=os.environ.get("HOST"),
    port=os.environ.get("PORT"),
)

user_db = UserDatabaseManager(
    dbname=os.environ.get("USERDB_NAME"),
    user=os.environ.get("USERDB_USER"),
    password=os.environ.get("USERDB_PASSWORD"),
    host=os.environ.get("HOST"),
    port=os.environ.get("PORT"),
)

streetmap_api = NominatimAPI()

app = Flask(__name__)
app.secret_key = os.environ.get("SECRET_KEY")


def logged_required(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        if "username" not in session:
            logger.info("No user in session redirecting to home page")
            return redirect(url_for("login"))
        return function(*args, **kwargs)

    return wrapper


def permission_required(permission):
    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            if permission not in session["user_permissions"]:
                logger.info(
                    f"Trying to get access to page that requires {permission} but user has {session['user_permissions']}"
                )
                return redirect(url_for("home"))
            return function(*args, **kwargs)

        return wrapper

    return decorator


def search_error(error):
    error_pattern = re.compile(r"'msg': '(.+?)',")
    errors = re.finditer(error_pattern, error)
    errors = " ".join([error.group(1) for error in errors])
    return errors


@app.route("/add_user", methods=["POST", "GET"])
@permission_required("add_user")
@logged_required
def add_user():
    position_info = {}
    if request.method == "POST" and not "position_name_search" in request.form:
        user_info = {
            "first_name": request.form.get("first_name"),
            "last_name": request.form.get("last_name"),
            "email": request.form.get("email"),
            "position_name": request.form.get("position_name"),
        }

        try:
            UserInput(**user_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/add_user")

        logger.info("Trying to add user {}, {}, {}, {}".format(*user_info.values()))
        user_db.add_user(user_info)
        flash("Successfully added user! 🤗", "success")
        return redirect(url_for("add_user"))
    elif request.method == "POST":
        position_info["position_name"] = request.form.get("position_name_search")
        positions = user_db.get_positions_with_permissions(position_info)
        return render_template("add_user.html", data=[positions])
    return render_template("add_user.html", data=[])


@app.route("/delete_user", methods=["POST", "GET"])
@permission_required("delete_user")
@logged_required
def delete_user():
    if request.method == "POST" and "id" in request.form:
        if request.method == "POST":
            user_info = {
                "id": request.form.get("id"),
                "first_name": request.form.get("first_name"),
                "last_name": request.form.get("last_name"),
                "email": request.form.get("email"),
            }

            try:
                UserInput(**user_info)
            except ValueError as e:
                flash(search_error(repr(e)), "danger")
                return redirect(f"/delete_user")

            user_db.delete_user(user_info)
            flash("Successfully deleted user! 🤗", "success")
            return render_template("delete_user.html")
    elif request.method == "POST":
        user_info = {
            "first_name": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            "last_name": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            "email": None
            if request.form.get("email") == ""
            else request.form.get("email"),
            "position_name": None
            if request.form.get("position_name") == ""
            else request.form.get("position_name"),
        }

        try:
            UserInput(**user_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/delete_user")

        user_info = user_db.get_user(user_info)
        return render_template("delete_user.html", data=user_info)
    return render_template("delete_user.html")


@app.route("/update_user", methods=["POST", "GET"])
@permission_required("update_user")
@logged_required
def update_user():
    if request.method == "POST" and "id" in request.form:
        user_info = {
            "id": request.form.get("id"),
            "first_name": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            "last_name": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            "email": None
            if request.form.get("email") == ""
            else request.form.get("email"),
            "password": None
            if request.form.get("password") == ""
            else request.form.get("password"),
            "position_name": None
            if request.form.get("position_name") == ""
            else request.form.get("position_name"),
        }
        logger.info(
            "Trying to update user with such data: {}, {}, {}".format(
                *user_info.values()
            )
        )

        try:
            UserInput(**user_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/update_user")

        user_db.update_user(user_info)
        flash("Successfully updated user! 🤗", "success")
        return render_template("update_user.html")
    elif request.method == "POST":
        user_info = {
            "first_name": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            "last_name": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            "email": None
            if request.form.get("email") == ""
            else request.form.get("email"),
            "position_name": None
            if request.form.get("position_name") == ""
            else request.form.get("position_name"),
        }

        try:
            UserInput(**user_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/update_user")

        user_info = user_db.get_user(user_info)
        return render_template("update_user.html", data=user_info)
    return render_template("update_user.html")


@app.route("/add_position", methods=["POST", "GET"])
@permission_required("add_position")
@logged_required
def add_position():
    permissions = user_db.get_uniq_permissions()
    position_info = {}
    if request.method == "POST":
        position_info["id"] = request.form.get("id")
        position_info["position_name"] = request.form.get("position_name")
        position_info["permissions"] = [
            element["name"]
            for element in permissions
            if element["name"] == request.form.get(element["name"])
        ]
        logger.info(f"add-position: {position_info}")
        if not (new_permissions := request.form.get("position_permission")) == "":
            position_info["permissions"].extend(new_permissions.split(";"))

        try:
            PositionInput(**position_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/add_position")

        logger.info(f"add-position: {position_info}")
        user_db.add_position(position_info)
        flash("Successfully added position(s)! 🤗", "success")
        return redirect(url_for("add_position"))
    return render_template("add_position.html", data=permissions)


@app.route("/update_position", methods=["POST", "GET"])
@permission_required("update_position")
@logged_required
def update_position():
    permissions = user_db.get_uniq_permissions()
    position_info = {}
    if request.method == "POST" and "id" in request.form:
        position_info["id"] = request.form.get("id")
        position_info["position_name"] = request.form.get("position_name")
        position_info["permissions"] = [
            element["name"]
            for element in permissions
            if element["name"] == request.form.get(element["name"])
        ]

        if not (new_permissions := request.form.get("position_permission")) == "":
            position_info["permissions"].extend(new_permissions.split(";"))

        try:
            PositionInput(**position_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/update_position")

        user_db.update_position_permission(position_info)
        flash("Successfully updated position! 🤗", "success")
        return render_template("update_position.html", data=[[], permissions, []])
    elif request.method == "POST":
        position_info["position_name"] = request.form.get("position_name")
        positions = user_db.get_positions_with_permissions(position_info)
        pos_perm = user_db.get_positions_with_permissions(position_info)

        try:
            PositionInput(**position_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/update_position")

        if pos_perm[0]["position_name"] == request.form.get("position_name"):
            pos_perm[0]["permission_name"] = [pos_perm[0]["permission_name"]]
            for element in pos_perm:
                pos_perm[0]["permission_name"].append(element["permission_name"])
            pos_perm = pos_perm[0]
            return render_template(
                "update_position.html", data=[positions, permissions, pos_perm]
            )
        else:
            return render_template(
                "update_position.html", data=[positions, permissions, []]
            )
    return render_template("update_position.html", data=[[], permissions, []])


@app.route("/delete_position", methods=["POST", "GET"])
@permission_required("delete_position")
@logged_required
def delete_position():
    if request.method == "POST" and "id" in request.form:
        position_info = {
            "id": request.form.get("id"),
            "position_name": request.form.get("position_name"),
        }

        try:
            PositionInput(**position_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/delete_position")

        user_db.delete_position_permission(position_info)
        flash("Successfully deleted position(s)! 🤗", "success")
        return render_template("delete_position.html")
    if request.method == "POST":
        position_info = {"position_name": request.form.get("position_name")}

        try:
            PositionInput(**position_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/delete_position")

        positions = user_db.get_positions_with_permissions(position_info)
        return render_template("delete_position.html", data=positions)
    return render_template("delete_position.html")


# @app.route("/add_client", methods=["POST", "GET"])
# @permission_required("add_client")
# @logged_required
# def add_client():
#     client_info = {}
#     if request.method == "POST":
#         client_info["first_name"] = request.form.get("first_name")
#         client_info["last_name"] = request.form.get("last_name")
#         client_info["email"] = request.form.get("email")
#         client_info["phone_number"] = request.form.get("phone_number")
#         client_info["company"] = request.form.get("company")

#         logger.info(f"add-client {client_info}")

#         delivery_db.add_client(client_info)

#         return render_template("add_client.html")
#     return render_template("add_client.html")


@app.route("/update_client", methods=["POST", "GET"])
@permission_required("update_client")
@logged_required
def update_client():
    if request.method == "POST" and "id" in request.form:
        client_info = {
            "id": request.form.get("id"),
            "first_name": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            "last_name": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            "email": None
            if request.form.get("email") == ""
            else request.form.get("email"),
            "phone_number": None
            if request.form.get("phone_number") == ""
            else request.form.get("phone_number"),
            "company": None
            if request.form.get("company") == ""
            else request.form.get("company"),
        }

        try:
            ClientInput(**client_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/update_client")

        delivery_db.update_client(client_info)
        flash("Sucessfully updated user! 🤗", "success")
        return render_template("update_client.html")
    elif request.method == "POST":
        client_info = {
            "first_name": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            "last_name": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            "email": None
            if request.form.get("email") == ""
            else request.form.get("email"),
            "phone_number": None
            if request.form.get("phone_number") == ""
            else request.form.get("phone_number"),
            "company": None
            if request.form.get("company") == ""
            else request.form.get("company"),
        }

        try:
            ClientInput(**client_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/update_client")

        client_info = delivery_db.get_client(client_info)
        return render_template("update_client.html", data=[client_info])
    elif request.method == "GET":
        id = request.args.get("id", default=None, type=int)

        if id != None:
            client_info = {"id": id}
            client_info = delivery_db.get_client(client_info)
            return render_template("update_client.html", data=[client_info])

    return render_template("update_client.html")


@app.route("/delete_client", methods=["POST", "GET"])
@permission_required("delete_client")
@logged_required
def delete_client():
    if request.method == "POST" and "id" in request.form:
        client_info = {
            "id": request.form.get("id"),
            "first_name": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            "last_name": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            "email": None
            if request.form.get("email") == ""
            else request.form.get("email"),
            "phone_number": None
            if request.form.get("phone_number") == ""
            else request.form.get("phone_number"),
            "company": None
            if request.form.get("company") == ""
            else request.form.get("company"),
        }

        try:
            ClientInput(**client_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/delete_client")

        delivery_db.delete_client(client_info)
        flash("Sucessfully deleted user 🤗", "success")
        return render_template("delete_client.html")
    elif request.method == "POST":
        client_info = {
            "first_name": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            "last_name": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            "email": None
            if request.form.get("email") == ""
            else request.form.get("email"),
            "phone_number": None
            if request.form.get("phone_number") == ""
            else request.form.get("phone_number"),
            "company": None
            if request.form.get("company") == ""
            else request.form.get("company"),
        }

        try:
            ClientInput(**client_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/delete_client")

        client_info = delivery_db.get_client(client_info)
        return render_template("delete_client.html", data=[client_info])
    return render_template("delete_client.html")


@app.route("/logout")
@logged_required
def log_out():
    session.clear()
    logger.info("Logged out, cleared session")
    return redirect(url_for("login"))


@app.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "POST":

        email, password = request.form.get("email"), request.form.get("password")
        logger.info(
            f"ROUTE: /login: trying to log in with email: {email} password: {password}"
        )
        # TO delete after tests
        # email = "grigorij.gajewski@dzimejl.com"
        # password = "mi"

        if user_id := user_db.get_user_login({"email": email, "password": password})[
            "id"
        ]:
            session["user_id"] = user_id
            session["username"] = email
            session["user_permissions"] = [
                element["name"] for element in user_db.get_user_permissions(user_id)
            ]

            logger.info(
                f"trying to log in session: user_id:{user_id}, username: {session['username']}, permissions: {session['user_permissions']}"
            )
            return redirect(url_for("home"))
        else:
            flash("Incorrect login or password! :(", "danger")
            return redirect(url_for("login"))

    return render_template("login.html")


@app.route("/reset_password/<code>", methods=["POST", "GET"])
def reset_password(code):
    if request.method == "POST":
        if request.form.get("password") == request.form.get("password_check"):

            try:
                UserInput(**{"password": request.form.get("password")})
            except ValueError as e:
                flash(search_error(e), "danger")
                redirect(f"/reset_password/{code}")

            user_db.reset_password(request.form.get("password"), code)
            flash("Successfully changed password! 🤗", "success")
            return redirect(url_for("login"))
    return (
        redirect(url_for("login"))
        if not int(user_db.reset_password_check(code)["case"])
        else render_template("reset_password.html")
    )


@app.route("/get_reset_link", methods=["POST", "GET"])
def get_reset_link():
    if request.method == "POST":
        user_db.send_reset_link(request.form.get("email"))
        return render_template("get_reset_link.html")
    return render_template("get_reset_link.html")


@app.route("/add_package")
@permission_required("add_package")
@logged_required
def route_package():
    temp = "add_package" + "/new"
    return redirect(temp)


@app.route("/add_package/<id>", methods=["POST", "GET"])
@permission_required("add_package")
@logged_required
def add_package(id):
    features = delivery_db.get_features_list("")
    discounts = delivery_db.get_discounts_list("")
    package_info = {}
    if request.method == "POST" and request.form.get("get_cost") == "True":
        package_info["order_id"] = request.form.get("order_id")
        package_info["x"] = request.form.get("x")
        package_info["y"] = request.form.get("y")
        package_info["z"] = request.form.get("z")
        package_info["weight"] = request.form.get("weight")
        package_info["feature"] = [
            element["feature"]
            for element in features
            if element["feature"] == request.form.get(element["feature"])
        ]
        package_info["discount"] = [
            element["discount"]
            for element in discounts
            if element["discount"] == request.form.get(element["discount"])
        ]

        try:
            PackageInput(**package_info)
        except ValueError as e:
            print(f'{type(package_info["x"])}')
            flash(search_error(repr(e)), "danger")
            return redirect(f"/add_package/{id}")
        try:
            package_info["cost"] = delivery_db.calc_cost(package_info)["cost"]
        except:
            package_info["cost"] = "ERROR"
        return render_template(
            "add_package.html", data=[id, features, discounts, package_info]
        )

    elif request.method == "POST":
        package_info["order_id"] = request.form.get("order_id")
        package_info["x"] = request.form.get("x")
        package_info["y"] = request.form.get("y")
        package_info["z"] = request.form.get("z")
        package_info["weight"] = request.form.get("weight")
        package_info["feature"] = [
            element["feature"]
            for element in features
            if element["feature"] == request.form.get(element["feature"])
        ]
        package_info["discount"] = [
            element["discount"]
            for element in discounts
            if element["discount"] == request.form.get(element["discount"])
        ]

        try:
            PackageInput(**package_info)
        except ValueError as e:
            print(f'{type(package_info["x"])}')
            flash(search_error(repr(e)), "danger")
            return redirect(f"/add_package/{id}")

        delivery_db.add_package(package_info)

        flash("Sucessfully added package! 🤗", "success")
        return redirect("/add_package/" + id)

    return render_template("add_package.html", data=[id, features, discounts])


@app.route("/manage_features", methods=["POST", "GET"])
@permission_required("manage_features")
@logged_required
def manage_features():
    if request.method == "POST":
        return render_template(
            "manage_features.html",
            data=delivery_db.get_features_list(request.form.get("feature_name")),
        )
    return render_template("manage_features.html")


@app.route("/manage_features_add", methods=["POST", "GET"])
@permission_required("manage_features")
@logged_required
def manage_features_add():
    if request.method == "POST":
        feature_add_list = {
            "Feature": request.form.get("feature_new_name"),
            "Rate": request.form.get("feature_new_rate"),
        }
        delivery_db.feature_add(feature_add_list)
        flash("Successfully added features! 🤗", "success")
        return render_template("manage_features.html")
    return render_template("manage_features.html")


@app.route("/manage_features_delete", methods=["POST", "GET"])
@permission_required("manage_features")
@logged_required
def manage_features_delete():
    features = delivery_db.get_features_list("")
    features_delete_list = []
    if request.method == "POST":
        features_delete_list = [
            element["feature"]
            for element in features
            if element["feature"] == request.form.get(element["feature"])
        ]
        delivery_db.features_delete(features_delete_list)
        flash("Successfully deleted features! 🤗", "success")
        return render_template("manage_features.html")
    return render_template("manage_features.html")


@app.route("/manage_features_update", methods=["POST", "GET"])
@permission_required("manage_features")
@logged_required
def manage_features_update():
    if request.method == "POST":
        features_update_list = {
            "ID": None
            if request.form.get("feature_id") == ""
            else request.form.get("feature_id"),
            "Feature": None
            if request.form.get("feature_new_name") == ""
            else request.form.get("feature_new_name"),
            "Rate": None
            if request.form.get("feature_new_rate") == ""
            else request.form.get("feature_new_rate"),
        }
        delivery_db.feature_update(features_update_list)
        flash("Successfully updated features! 🤗", "success")
        return render_template("manage_features.html")
    return render_template("manage_features.html")


@app.route("/manage_discounts", methods=["POST", "GET"])
@permission_required("manage_discounts")
@logged_required
def manage_discounts():
    if request.method == "POST":
        return render_template(
            "manage_discounts.html",
            data=delivery_db.get_discounts_list(request.form.get("discount")),
        )
    return render_template("manage_discounts.html")


@app.route("/manage_discounts_add", methods=["POST", "GET"])
@permission_required("manage_discounts")
@logged_required
def manage_discounts_add():
    if request.method == "POST":
        discount_add_list = {
            "Discount": request.form.get("discount_new_name"),
            "Rate": request.form.get("discount_new_rate"),
        }
        delivery_db.discount_add(discount_add_list)
        flash("Successfully added discounts! 🤗", "success")
        return render_template("manage_discounts.html")
    return render_template("manage_discounts.html")


@app.route("/manage_discounts_delete", methods=["POST", "GET"])
@permission_required("manage_discounts")
@logged_required
def manage_discounts_delete():
    discounts = delivery_db.get_discounts_list("")
    discounts_delete_list = []
    if request.method == "POST":
        discounts_delete_list = [
            element["discount"]
            for element in discounts
            if element["discount"] == request.form.get(element["discount"])
        ]
        delivery_db.discounts_delete(discounts_delete_list)
        flash("Successfully deleted discounts! 🤗", "success")
        return render_template("manage_discounts.html")
    return render_template("manage_discounts.html")


@app.route("/manage_discounts_update", methods=["POST", "GET"])
@permission_required("manage_discounts")
@logged_required
def manage_discounts_update():
    if request.method == "POST":
        discounts_update_list = {
            "ID": None
            if request.form.get("discount_id") == ""
            else request.form.get("discount_id"),
            "Discount": None
            if request.form.get("discount_new_name") == ""
            else request.form.get("discount_new_name"),
            "Rate": None
            if request.form.get("discount_new_rate") == ""
            else request.form.get("discount_new_rate"),
        }
        delivery_db.discount_update(discounts_update_list)
        flash("Successfully updated discounts! 🤗", "success")
        return render_template("manage_discounts.html")
    return render_template("manage_discounts.html")


@app.route("/search_order", methods=["POST", "GET"])
@permission_required("search_order")
@logged_required
def search_order():
    if request.method == "POST":
        order_info = {
            "order_id": None
            if request.form.get("order_id") == ""
            else request.form.get("order_id"),
            "first_name": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            "last_name": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            "phone_number": None
            if request.form.get("phone") == ""
            else request.form.get("phone"),
            "company": None
            if request.form.get("company") == ""
            else request.form.get("company"),
            "email": None
            if request.form.get("email") == ""
            else request.form.get("email") == "",
            "city": None
            if request.form.get("add_dest") == ""
            else request.form.get("add_dest"),
            "status_name": None
            if request.form.get("order_status") == ""
            else request.form.get("order_status"),
            "shipping_date": "1970-01-01"
            if request.form.get("shipping_date") == ""
            else request.form.get("shipping_date"),
            "delivery_date": "1970-01-01"
            if request.form.get("delivery_date") == ""
            else request.form.get("delivery_date"),
            "station_name": None
            if request.form.get("current_station") == ""
            else request.form.get("current_station"),
        }
        return render_template(
            "search_order.html", data=delivery_db.search_order(order_info)
        )
    return render_template("search_order.html")


@app.route("/api/delete_package/<text_order_id>/<package_id>", methods=["POST", "GET"])
@permission_required("delete_package")
@logged_required
def delete_package(text_order_id, package_id):
    delivery_db.delete_package({"id": package_id})
    flash("Successfully removed package from database! 🤗", "success")
    return redirect("/order/show/" + text_order_id)


# Dla skanera:
@app.route("/api/update_station", methods=["POST", "GET"])
@permission_required(
    "update_status"
)  # TODO add update_status and position scanner to db
@logged_required
def update_package_status():
    if request.method == "GET":
        if (
            "station_id" in request.args
            and "text_order_id" in request.args
            and "status" in request.args
        ):

            station_id = request.args["station_id"]
            text_order_id = request.args["text_order_id"]
            new_status_name = request.args["status"]

            order_data = delivery_db.get_order({"order_id": text_order_id})
            if "id" in order_data:
                order_id = order_data["id"]

                new_state = delivery_db.get_state({"status_name": new_status_name})
                if not "id" in new_state:
                    return "{Status: Fail, Reason:'No such status'}"
                new_state_id = new_state["id"]

                if new_state_id == None:
                    return "{Status: Fail, Reason:'No such status'}"
                delivery_db.update_order(
                    {
                        "id": order_id,
                        "current_station": station_id,
                        "order_status": new_state_id,
                    }
                )

                delivery_db.add_delivery_history(
                    {
                        "id": order_id,
                        "current_station": station_id,
                        "order_status": new_state_id,
                    }
                )

                return "{Status: OK}"
    return "{Status: Fail}"


@app.route("/update_delivery_address/<text_order_id>", methods=["POST", "GET"])
@permission_required("add_order")
@logged_required
def update_delivery_address(text_order_id):
    delivery_info = delivery_db.get_order({"order_id": text_order_id})
    order_id = delivery_info["id"]
    if request.method == "POST":

        address_info = {
            f"apartment_number": None
            if request.form.get("apartment_number") == ""
            else request.form.get("apartment_number"),
            f"street": None
            if request.form.get("street") == ""
            else request.form.get("street"),
            f"postal_code": None
            if request.form.get("postal_code") == ""
            else request.form.get("postal_code"),
            f"city": None
            if request.form.get("city") == ""
            else request.form.get("city"),
        }

        if not (address_info := streetmap_api.search(address_info)):
            flash(
                "Podane dane adresowe są nieprawidłowe lub nie znajdują się w ogólnodostępnych bazach adresowych!",
                "danger",
            )
        else:
            address_info["order_id"] = order_id
            delivery_db.add_address(address_info)
        return redirect("/order/show/"+text_order_id)
    return render_template("update_delivery_address.html")
    # return redirect(f"")


@app.route("/order/show/<text_order_id>", methods=["POST", "GET"])
def show_order(text_order_id):

    if request.method == "POST":
        delivery_info = delivery_db.get_order({"order_id": text_order_id})

        order_id = delivery_info["id"]

        if "status_name" in request.form:
            status_name = request.form["status_name"]
            new_state_id = delivery_db.get_state({"status_name": status_name})
            delivery_db.update_order({"id": order_id, "order_status": new_state_id})

        if "shipping date" in request.form:
            shipping_date = request.form["shipping date"]
            delivery_db.update_order({"id": order_id, "shipping_date": shipping_date})

        if "delivery date" in request.form:
            delivery_date = request.form["delivery date"]
            delivery_db.update_order({"id": order_id, "delivery_date": delivery_date})

        return redirect("/order/show/" + text_order_id, code=302)

    if request.method == "POST" or request.method == "GET":

        delivery_info = delivery_db.get_order({"order_id": text_order_id})

        order_id = delivery_info["id"]

        order_info = {"order_id": order_id}

        is_edit_mode = False
        is_logged = "user_id" in session

        is_user_allowed_to_edit = False
        if is_logged:
            is_user_allowed_to_edit = "edit_order" in session["user_permissions"]
            if request.method == "GET" and is_user_allowed_to_edit:
                is_edit_mode = request.args.get("edit", default=False, type=bool)

        possible_states = delivery_db.get_possible_states()
        delivery_history = delivery_db.get_delivery_history(order_info)
        delivery_display_info = {}
        edit_links = {}
        field_type = {}

        delivery_display_info["order id"] = text_order_id
        field_type["order id"] = "const"

        # Getting delivery address info:
        delivery_display_info["delivery address"] = delivery_db.get_address(
            {"id": delivery_info["delivery_address"]}
        )
        edit_links["delivery address"] = "/update_delivery_address/" + text_order_id
        field_type["delivery address"] = "redirect"

        # Getting shipping address info:
        delivery_display_info["shipping address"] = delivery_db.get_address(
            {"id": delivery_info["shipping_address"]}
        )
        field_type["shipping address"] = "const"

        delivery_display_info["state"] = delivery_db.get_state(
            {"id": delivery_info["order_status"]}
        )
        field_type["state"] = "inplace_list"

        delivery_display_info["sender"] = delivery_db.get_client(
            {"id": delivery_info["sender"]}
        )
        edit_links["sender"] = f"/update_client?id={delivery_info['sender']}"
        field_type["sender"] = "redirect"

        delivery_display_info["recipient"] = delivery_db.get_client(
            {"id": delivery_info["recipient"]}
        )
        edit_links["recipient"] = f"/update_client?id={delivery_info['recipient']}"
        field_type["recipient"] = "redirect"

        delivery_display_info["shipping date"] = delivery_info["shipping_date"]
        field_type["shipping date"] = "inplace_date"

        delivery_display_info["delivery date"] = delivery_info["delivery_date"]
        field_type["delivery date"] = "inplace_date"

        delivery_display_info["current station"] = delivery_db.get_station(
            {"id": delivery_info["current_station"]}
        )[0]
        field_type["current station"] = "redirect"

        delivery_history = [
            {
                "station": delivery_db.get_station({"id": history["station_id"]}),
                "time": history["step"].strftime("%b %d %Y %H:%M"),
                "status": delivery_db.get_state({"id": history["order_status"]})[
                    "status_name"
                ],
            }
            for history in delivery_history
        ]

        (display_info_client,) = delivery_db.get_order_displayinfo(text_order_id)

        packages_infos = delivery_db.get_package({"order_id": text_order_id})
        packages_links = [
            ("/order/show/" + text_order_id + "/packages/" + str(package["id"]))
            for package in packages_infos
        ]
        packages_delete_links = [
            ("/api/delete_package/" + text_order_id + "/" + str(package["id"]))
            for package in packages_infos
        ]
        print(delivery_info)
        return render_template(
            "show_order.html",
            data={
                "delivery_history": delivery_history,
                "delivery_info": delivery_info,
                "delivery_display_info": delivery_display_info,
                "display_info_client": display_info_client,
                "edit_links": edit_links,
                "packages_links": packages_links,
                "packages_delete_links": packages_delete_links,
                "is_logged": is_logged,
                "is_edit_mode": is_edit_mode,
                "field_type": field_type,
                "possible_states": possible_states,
                "is_user_allowed_to_edit": is_user_allowed_to_edit,
            },
        )
    # return render_template("show_order.html", data=delivery_history)


@app.route("/order/show/<order_id>/packages/<package_id>", methods=["POST", "GET"])
def show_package(order_id, package_id):

    package_info = delivery_db.get_package({"id": package_id})[0]
    print(package_info)
    package_display_info = {}
    package_display_info["order id"] = order_id
    (dim,) = delivery_db.get_dimensions(package_info["dimensions_id"])
    package_display_info["dimentions"] = f'{dim["x"]}cm x {dim["y"]}cm x {dim["z"]}cm'
    package_display_info["weight"] = f'{package_info["package_weight"]} kg'
    package_display_info["cost"] = f'{package_info["cost"]} PLN'

    return render_template(
        "show_package.html", data={"package_info": package_display_info}
    )


@app.route("/search_order_code", methods=["POST", "GET"])
def search_order_code():
    if request.method == "POST":
        if "order_id" in request.form:
            order_id = request.form["order_id"]
            out = delivery_db.get_order({"order_id": order_id})
            if out["id"] == None:
                flash("Wprowadzony numer zamówienia nie istnieje!", "danger")
            else:
                return redirect("/order/show/" + order_id)
    return render_template("search_order_code.html")


@app.route("/")
def global_home():
    return render_template("index.html")


@app.route("/home")
@logged_required
def home():
    # return f"Cześć drogi użytkowniku {session['username']} twoje uprawnienia to {session['user_permissions']}"
    user_info = user_db.get_user({"email": session["username"]})[0]

    return render_template(
        "home.html",
        data={
            "displayname": user_info["first_name"] + " " + user_info["last_name"],
            "permissions": permissions_mapping(session["user_permissions"]),
        },
    )


####################################################################################
##################### ORDER PIPELINE ###############################################


@app.route("/add_order")
@permission_required("add_order")
@logged_required
def route_order():
    temp = "add_order" + "/sender"
    return redirect(temp)


@app.route("/add_order/<kind>", methods=["POST", "GET"])
@logged_required
@permission_required("add_order")
def add_order(kind):
    if request.method == "POST":
        client_info = {
            f"email_{kind}": None
            if request.form.get("email") == ""
            else request.form.get("email"),
            f"phone_number_{kind}": None
            if request.form.get("phone_number") == ""
            else request.form.get("phone_number"),
            f"first_name_{kind}": None
            if request.form.get("first_name") == ""
            else request.form.get("first_name"),
            f"last_name_{kind}": None
            if request.form.get("last_name") == ""
            else request.form.get("last_name"),
            f"company_{kind}": None
            if request.form.get("company") == ""
            else request.form.get("company"),
        }
        try:
            ClientInput(**client_info)
        except ValueError as e:
            flash(search_error(repr(e)), "danger")
            return redirect(f"/order/add_client/{kind}")
            # flash(search_error(str(repr(e))), "danger")

        # if not (client := delivery_db.add_client(client_info)):
        #     session[kind] = delivery_db.add_client(client_info)
        # else:
        #     session[kind] = client["id"]

        address_info = {
            f"apartment_number_{kind}": None
            if request.form.get("apartment_number") == ""
            else request.form.get("apartment_number"),
            f"street_{kind}": None
            if request.form.get("street") == ""
            else request.form.get("street"),
            f"postal_code_{kind}": None
            if request.form.get("postal_code") == ""
            else request.form.get("postal_code"),
            f"city_{kind}": None
            if request.form.get("city") == ""
            else request.form.get("city"),
        }

        if not (address_info := streetmap_api.search(address_info)):
            flash(
                "Podane dane adresowe są nieprawidłowe lub nie znajdują się w ogólnodostępnych bazach adresowych!",
                "danger",
            )
            return redirect(f"{kind}")

        keys = list(address_info.keys())
        for key in keys:
            address_info[f"{key}_{kind}"] = address_info.pop(key)

        client_info.update(address_info)

        session[kind] = client_info

        if kind == "sender":
            return redirect("recipient")

        else:
            current_station = delivery_db.get_station(
                    {"station_name": "Baza główna Paczoocha"}
                )
            
            
            if isinstance(current_station, list ):
                current_station_id = current_station[0]["id"]
            else:
                current_station_id = current_station["id"]
            

            order_info = {
                "current_station": current_station_id
            }
            order_info.update(session["sender"])
            order_info.update(session["recipient"])
            order_info = delivery_db.add_order(order_info)
            if not order_info:
                flash("Adding order error, redirecting...", "danger")
                return redirect("/add_order/sender")

            delivery_db.add_delivery_history(order_info)
            session["order_id"] = order_info["order_id"]

            flash("Pomyślnie dodano zamówienie", "success")
            return redirect("/add_package/" + session["order_id"])

    return render_template("add_client.html", data=kind)


####################################################################################
####################################################################################


if __name__ == "__main__":

    app.run(debug=False)
    user_db.close()
    delivery_db.close()
