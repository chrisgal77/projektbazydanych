import faker

fake = faker.Faker(["pl_PL"])


def get_fake_user(times):
    for _ in range(times):
        yield {"first_name": fake.first_name(), "last_name": fake.last_name()}


def get_fake_package():
    pass


def get_fake_address(times):
    for _ in range(times):
        yield {
            "city": fake.city(),
            "postcode": fake.postcode(),
            "address": fake.street(),
            "building_number": fake.building_number(),
        }


def get_fake_feature():
    pass


if __name__ == "__main__":
    for a in get_fake_address(1):
        print(a)
