from lib2to3.pytree import Base
import re
from typing import List, Optional

from pydantic import BaseModel, validator


class ClientInput(BaseModel):

    id: Optional[str]
    email: Optional[str]
    phone_number: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    company: Optional[str]

    @validator("email")
    def validate_email(cls, value: Optional[str]) -> Optional[str]:
        if not value:
            return value
        if not re.search(
            r"([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(.[A-Z|a-z]{2,})+", value
        ):
            raise ValueError("Incorrect email format!")
        if len(value) > 100:
            raise ValueError("Too many characters!")

    @validator("phone_number")
    def validate_phone_number(cls, value: Optional[str]) -> Optional[str]:
        if not value:
            return value
        if len(value) != 9:
            raise ValueError("Incorrect phone number!")
        if not value.isdigit():
            raise ValueError("Given phone number is not a number")
        return value

    @validator("first_name", "last_name")
    def validate_names(cls, value: Optional[str]) -> Optional[str]:
        if not value:
            return value
        if len(value) > 35:
            raise ValueError("Name too long!")

        return value


class PackageInput(BaseModel):
    order_id: Optional[str]
    x: Optional[str]
    y: Optional[str]
    z: Optional[str]
    weight: Optional[str]
    feature: Optional[List[str]]
    discount: Optional[List[str]]

    @validator("x")
    def validate_x(value: Optional[str]):
        if not value:
            return value
        if not value.isdigit():
            raise ValueError("Length must be a number!")

        if int(value) > 150 or int(value) < 10:
            raise ValueError("Incorrect package length! Length should be less than 150cm and higher than 10cm")
        return value

    @validator("y")
    def validate_y(value: Optional[str]):
        if not value:
            return value
        if not value.isdigit():
            raise ValueError("Width must be a number!")
        if int(value) > 150 or int(value) < 10:
            raise ValueError("Incorrect package width! Width should be less than 150cm and higher than 10cm")
        return value

    @validator("z")
    def validate_z(value: Optional[str]):
        if not value:
            return value
        if not value.isdigit():
            raise ValueError("Height must be a number!")
        if int(value) > 150 or int(value) < 10:
            raise ValueError("Incorrect package height! Height should be less than 150cm and higher than 10cm")
        return value

    @validator("weight")
    def validate_weight(value: Optional[str]):
        if not value:
            return value
        if not value.isdigit():
            raise ValueError("Weight must be a number!")
        if int(value) > 50 or int(value) < 0:
            raise ValueError("Incorrect package weight! Weight should be less than 50kg and higher than 0")
        return value


class UserInput(BaseModel):
    id: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    position_name: Optional[str]
    password: Optional[str]

    @validator("email")
    def validate_email(cls, value: Optional[str]) -> Optional[str]:
        if not value:
            return value
        if not re.search(
            r"([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(.[A-Z|a-z]{2,})+", value
        ):
            raise ValueError("Incorrect email format!")
        if len(value) > 100:
            raise ValueError("Too many characters! We handle less than 100 characters")

    @validator("position_name")
    def validate_postion_name(cls, value: Optional[str]) -> Optional[str]:
        if not value:
            return value
        if len(value) > 35:
            raise ValueError("Position name too long! Max 35")
        return value

    @validator("first_name", "last_name")
    def validate_names(cls, value: Optional[str]) -> Optional[str]:
        if not value:
            return value
        if len(value) > 35:
            raise ValueError("Name too long! Max 35")

        return value

    @validator("password")
    def validate_password(value: str) -> str:
        if len(value) < 8:
            raise ValueError("Password must have at least 8 characters!")
        if not any(char.isdigit() for char in value):
            raise ValueError("Password must have at least 1 number!")

        return value

class PositionInput(BaseModel):
    id: Optional[str]
    position_name: Optional[str]
    permissions: Optional[List[str]]

    @validator("position_name")
    def validate_position_name(value: Optional[str]):
        if not value:
            return value

        if len(value) > 35:
            raise ValueError("Too long position name! max 35")
        return value

    @validator("permissions")
    def validate_permissions(value: List[str]):
        if not value:
            return value

        for element in value:
            if len(element) > 35:
                raise ValueError(f"Too long permission name - {element}! Max 35")

        return value
