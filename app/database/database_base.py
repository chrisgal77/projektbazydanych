import logging
import sys

import psycopg2

logger = logging.getLogger("app")


class DatabaseManager:
    def __init__(self, dbname, user, password, host, port):

        self.connection_params = {
            "dbname": dbname,
            "user": user,
            "password": password,
            "host": host,
            "port": port,
            "sslmode": "require",
        }

        self.connect()

        self.conn.autocommit = True
        self.cur = self.conn.cursor()

    def connect(self):
        try:
            self.conn = psycopg2.connect(
                **self.connection_params,
                keepalives=0,
            )
            logger.info(f"Connected to {self.connection_params['dbname']} database")
            self.conn.autocommit = True
            self.cur = self.conn.cursor()
        except psycopg2.OperationalError:
            logger.info(
                f"Failed to connect {self.connection_params['dbname']} database"
            )
            sys.exit(1)

    def connected(self):
        return self.conn.closed

    def close(self):
        logger.info(
            f"Closed connection with {self.connection_params['dbname']} database"
        )
        self.conn.close()
