import logging
import os
import pathlib

import psycopg2

from .database_base import DatabaseManager
from .database_decorators import eof_handler, getter
from password_utils.password_handler import EmailSender

logger_name = pathlib.Path(__file__).stem
logger = logging.getLogger(logger_name)


class DeliveryDatabaseManager(DatabaseManager):
    def __init__(self, dbname, user, password, host, port):
        super().__init__(dbname, user, password, host, port)

        self.email_sender = EmailSender()

        self.database_features = {
            "order": (
                "id",
                "order_id",
                "package",
                "delivery_address",
                "shipping_address",
                "order_status",
                "sender",
                "recipient",
                "shipping_date",
                "delivery_date",
                "current_station",
            ),
            "state": (
                "id",
                "status_name",
            ),
            "package": ("id", "order_id", "dimensions_id", "package_weight", "cost"),
            "client": (
                "id",
                "email",
                "phone_number",
                "first_name",
                "last_name",
                "company",
            ),
            "station": ("id", "station_name", "station_address", "city"),
            "address": (
                "id",
                "house_number",
                "road",
                "suburb",
                "city",
                "state",
                "postcode",
                "country",
                "country_code",
            ),
        }

    def _validate(self, info, features):
        for key in features:
            if key not in info.keys():
                info[key] = None
            else:
                info[key] = None if info[key] == "" else info[key]
        return info

    @getter(
        "id order_id delivery_address shipping_address order_status sender recipient shipping_date delivery_date current_station cost",
        return_single=True,
    )
    @eof_handler
    def get_order(self, order_info):

        order_info = self._validate(order_info, self.database_features["order"])

        sql = """
            SELECT id, order_id, delivery_address, shipping_address, order_status, sender, recipient, shipping_date, delivery_date, current_station
            FROM orders WHERE
            (id=%(id)s OR %(id)s IS NULL) AND 
            (order_id=%(order_id)s OR %(order_id)s IS NULL) AND 
            (delivery_address=%(delivery_address)s OR %(delivery_address)s IS NULL) AND
            (shipping_address=%(shipping_address)s OR %(shipping_address)s IS NULL) AND
            (order_status=%(order_status)s OR %(order_status)s IS NULL) AND 
            (sender=%(sender)s OR %(sender)s IS NULL) AND
            (recipient=%(recipient)s OR %(recipient)s IS NULL) AND
            (shipping_date=%(shipping_date)s OR %(shipping_date)s IS NULL) AND
            (delivery_date=%(delivery_date)s OR %(delivery_date)s IS NULL) AND
            (current_station=%(current_station)s OR %(current_station)s IS NULL)
        """

        self.cur.execute(sql, order_info)

        return self.cur.fetchall()

    @getter("id order_id current_station order_status", return_single=True)
    # @eof_handler
    def add_order(self, order_info):

        order_info = self._validate(
            order_info,
            [f"{element}_sender" for element in self.database_features["address"]],
        )
        order_info = self._validate(
            order_info,
            [f"{element}_recipient" for element in self.database_features["address"]],
        )

        order_info["initial_state"] = self.get_state(
            {"status_name": str(os.environ.get("INITIAL_ORDER_STATE"))}
        )["id"]

        
        sql = """"""

        sender = self.get_client(
            {
                key_sender.replace("_sender", ""): order_info[key_sender]
                for key_sender in [
                    f"{element}_sender" for element in self.database_features["client"]
                ]
            }
        )["id"]
        if not sender:
            sql += """WITH sender_clients AS(
            INSERT INTO clients (email, phone_number, first_name, last_name, company)
            VALUES (%(email_sender)s, %(phone_number_sender)s, %(first_name_sender)s, %(last_name_sender)s, %(company_sender)s) RETURNING id),

            """
        else:
            order_info["sender_id"] = sender
            sql += """WITH sender_clients AS(
                SELECT id FROM clients WHERE id=%(sender_id)s),

            """
        address_sender = self.get_address(
            {
                address_sender.replace("_sender", ""): order_info[address_sender]
                for address_sender in [
                    f"{element}_sender" for element in self.database_features["address"]
                ]
            }
        )
        if isinstance(address_sender, list):
            address_sender_id = address_sender[0]["id"]
        else:
            address_sender_id = address_sender["id"]

        if not address_sender_id:
            sql += """sender_address AS(
            INSERT INTO addresses (house_number, road, suburb, city, state, postcode, country, country_code)
            VALUES (%(house_number_sender)s, %(road_sender)s, %(suburb_sender)s, %(city_sender)s, %(state_sender)s, %(postcode_sender)s, %(country_sender)s, %(country_code_sender)s) RETURNING id),

            """
        else:
            order_info["address_sender_id"] = address_sender_id
            sql += """sender_address AS(
                SELECT id FROM addresses WHERE id=%(address_sender_id)s),

            """

        recipient = self.get_client(
            {
                key_recipient.replace("_recipient", ""): order_info[key_recipient]
                for key_recipient in [
                    f"{element}_recipient"
                    for element in self.database_features["client"]
                ]
            }
        )["id"]
        if not recipient:
            sql += """recipient_clients AS(
            INSERT INTO clients (email, phone_number, first_name, last_name, company)
            VALUES (%(email_recipient)s, %(phone_number_recipient)s, %(first_name_recipient)s, %(last_name_recipient)s, %(company_recipient)s) RETURNING id),

            """
        else:
            order_info["recipient_id"] = recipient
            sql += """recipient_clients AS(
                SELECT id FROM clients WHERE id= %(recipient_id)s),

            """
        address_recipient = self.get_address(
            {
                address_recipient.replace("_recipient", ""): order_info[
                    address_recipient
                ]
                for address_recipient in [
                    f"{element}_recipient"
                    for element in self.database_features["address"]
                ]
            }
        )
        if isinstance(address_recipient, list):
            address_recipient_id = address_recipient[0]["id"]
        else:
            address_recipient_id = address_recipient["id"]

        if not address_recipient_id:
            sql += """recipient_address AS(
            INSERT INTO addresses (house_number, road, suburb, city, state, postcode, country, country_code)
            VALUES (%(house_number_recipient)s, %(road_recipient)s, %(suburb_recipient)s, %(city_recipient)s, %(state_recipient)s, %(postcode_recipient)s, %(country_recipient)s, %(country_code_recipient)s) RETURNING id)

            """
        else:
            order_info["address_recipient_id"] = address_recipient_id
            sql += """recipient_address AS(
                SELECT id FROM addresses WHERE id=%(address_recipient_id)s)
            """

        sql += """INSERT INTO orders (delivery_address, shipping_address, order_status, sender, recipient, shipping_date, delivery_date, current_station)
            SELECT recipient_address.id, sender_address.id, %(initial_state)s, sender_clients.id, recipient_clients.id, CURRENT_DATE, CURRENT_DATE + INTERVAL '3 DAY', %(current_station)s FROM
            sender_clients, sender_address, recipient_clients, recipient_address RETURNING orders.id, orders.order_id;
        """

        print(sql)
        self.cur.execute(sql, order_info)

        rv = [*self.cur.fetchall()[0]]

        self.email_sender.send_order_number(order_info['email_sender'], rv[1])
        self.email_sender.send_order_number(order_info['email_recipient'], rv[1])

        rv.extend([order_info["current_station"], order_info["initial_state"]])
        return [
            rv,
        ]

    @eof_handler
    def update_order(self, order_info):

        if not "id" in order_info:
            raise Exception("Order cant be identified!")

        order_original = self.get_order({"id": order_info["id"]})

        for key, value in order_info.items():
            if key not in order_original:
                continue

            if value != order_original[key]:
                order_original[key] = value

        sql = """ UPDATE orders SET delivery_address=%(delivery_address)s, shipping_address=%(shipping_address)s,
            order_status=%(order_status)s, sender=%(sender)s, recipient=%(recipient)s, delivery_date=%(delivery_date)s, current_station=%(current_station)s
            WHERE id=%(id)s
        """

        self.cur.execute(sql, order_original)

    def delete_order(self):
        raise NotImplementedError

    @getter("id order_id dimensions_id package_weight cost")
    @eof_handler
    def get_package(self, package_info):

        package_info = self._validate(package_info, self.database_features["package"])

        sql = """
            SELECT id, order_id, dimensions_id, package_weight, cost FROM packages
            WHERE (id=%(id)s OR %(id)s IS NULL) AND 
            (order_id=%(order_id)s OR %(order_id)s IS NULL) AND 
            (dimensions_id=%(dimensions_id)s OR %(dimensions_id)s IS NULL) AND
            (package_weight=%(package_weight)s OR %(package_weight)s IS NULL) AND
            (cost=%(cost)s OR %(cost)s IS NULL) 
        """

        self.cur.execute(sql, package_info)

        return self.cur.fetchall()

    @eof_handler
    def get_possible_states(self):
        self.cur.execute("SELECT status_name FROM states")
        return [val[0] for val in self.cur.fetchall()]

    @eof_handler
    def delete_package(self, package_info):

        sql = """
            DELETE FROM features WHERE package_id = %(id)s;
            DELETE FROM discounts WHERE package_id = %(id)s;
            DELETE FROM packages WHERE id = %(id)s;
            DELETE FROM dimensions WHERE id = (SELECT dimensions_id FROM packages WHERE id = %(id)s);
            
        """

        self.cur.execute(sql, package_info)
        return True

    @eof_handler
    @getter("x y z")
    def get_dimensions(self, dimensions_id):

        sql = """
            SELECT x,y,z FROM dimensions
            WHERE id=%(id)s
        """

        self.cur.execute(sql, {"id": dimensions_id})

        return self.cur.fetchall()

    @getter("cost", return_single=True)
    @eof_handler
    def calc_cost(self, package_info):

        sql = """
              SELECT calc_cost(%(x)s, %(y)s, %(z)s, %(weight)s, %(feature)s, %(discount)s)
              """

        self.cur.execute(sql, package_info)

        return self.cur.fetchall()

    @eof_handler
    def add_package(self, package_info):

        args = [package_info["x"], package_info["y"], package_info["z"]]
        sql = "INSERT INTO dimensions VALUES (DEFAULT, %s, %s, %s);"

        args.extend(
            [
                package_info["order_id"],
                package_info["weight"],
                package_info["x"],
                package_info["y"],
                package_info["z"],
                package_info["weight"],
                package_info["feature"],
                package_info["discount"],
            ]
        )
        sql += """
            INSERT INTO packages VALUES (DEFAULT, %s, currval('dimensions_id_seq'), %s, 
                                         calc_cost(%s, %s, %s, %s, %s, %s));
        """

        for element in package_info["feature"]:
            args.append(element)
            sql += """
                INSERT INTO features VALUES (DEFAULT, currval('packages_id_seq'),
                                             (SELECT id FROM features_list WHERE feature=%s));
            """

        for element in package_info["discount"]:
            args.append(element)
            sql += """
                INSERT INTO discounts VALUES (DEFAULT, currval('packages_id_seq'),
                                             (SELECT id FROM discounts_list WHERE discount_name=%s));
            """

        self.cur.execute(sql, args)

    @eof_handler
    def update_package(self, package_info):

        package_original = self.get_package(package_info)

        for key, value in package_info.items():
            if value != package_original[key]:
                package_original[key] = value

        sql = """
            UPDATE features SET order_id=%(order_id)s, dimensions=%(dimensions)s, package_weight=%(package_weight)s, cost=%(cost)s
            WHERE id=%(id)s
        """

        self.cur.execute(sql, package_info)

    @getter("id email phone_number first_name last_name company", return_single=True)
    @eof_handler
    def get_client(self, client_info):

        client_info = self._validate(client_info, self.database_features["client"])

        sql = """
            SELECT id, email, phone_number, first_name, last_name, company FROM clients
            WHERE (id=%(id)s OR %(id)s IS NULL) AND 
            (email=%(email)s OR %(email)s IS NULL) AND 
            (phone_number=%(phone_number)s OR %(phone_number)s IS NULL) AND
            (first_name=%(first_name)s OR %(first_name)s IS NULL) AND
            (last_name=%(last_name)s OR %(last_name)s IS NULL) AND
            (company=%(company)s OR %(company)s IS NULL)
        """

        self.cur.execute(sql, client_info)

        return self.cur.fetchall()

    @eof_handler
    def update_client(self, client_info):

        client_original = self.get_client({"id": client_info["id"]})
        for key, value in client_info.items():
            if value not in ["", None]:
                client_original[key] = value

        sql = """
            UPDATE clients SET email=%(email)s, phone_number=%(phone_number)s, first_name=%(first_name)s, last_name=%(last_name)s, company=%(company)s 
            WHERE id=%(id)s
        """

        self.cur.execute(sql, client_original)

    @eof_handler
    def delete_client(self, client_info):

        sql = """
            DELETE FROM clients WHERE first_name=%(first_name)s AND last_name=%(last_name)s AND email=%(email)s AND id=%(id)s
        """

        self.cur.execute(sql, client_info)

    @getter("id station_name road house_number city")
    @eof_handler
    def get_station(self, station_info):

        station_info = self._validate(station_info, self.database_features["station"])

        sql = """
            SELECT stations.id, stations.station_name, addresses.road, addresses.house_number, addresses.city FROM stations
            LEFT JOIN addresses ON stations.station_address=addresses.id WHERE
            (stations.id=%(id)s OR %(id)s IS NULL) AND
            (stations.station_name=%(station_name)s OR %(station_name)s IS NULL) AND
            (stations.station_address=%(station_address)s OR %(station_address)s IS NULL) AND
            (addresses.city=%(city)s OR %(city)s IS NULL)
        """

        self.cur.execute(sql, station_info)

        return self.cur.fetchall()

    @eof_handler
    def add_station(self, station_info, address_info):

        self.cur.execute("SELECT * FROM ")
        sql = """
            INSERT INTO stations (name, station_address) VALUES
            (%(name)s, %(station_address)s)
        """

        self.cur.execute(sql, station_info)

    @eof_handler
    def update_station(self, station_info):

        station_original = self.get_station({"id": station_info["id"]})
        for key, value in station_info.items():
            if value not in ["", None]:
                station_original[key] = value

        sql = """
                UPDATE stations SET name=%(name)s, station_address=%(station_address)s 
                WHERE id=%(id)s
        """

        self.cur.execute(sql, station_original)

    @eof_handler
    def delete_station(self, station_info):
        pass

    @getter("id status_name", return_single=True)
    @eof_handler
    def get_state(self, state_info):

        state_info = self._validate(state_info, self.database_features["state"])

        sql = """
            SELECT id, status_name FROM states 
            WHERE (id=%(id)s OR %(id)s IS NULL) AND
            (status_name=%(status_name)s OR %(status_name)s IS NULL)
        """

        self.cur.execute(sql, state_info)

        return self.cur.fetchall()

    @eof_handler
    def add_state(self, state_info):
        print("State info:", state_info)
        sql = """
            INSERT INTO states (status_name)
            VALUES (%(status_name)s) RETURNING id
        """
        try:
            self.cur.execute(sql, state_info)
        except psycopg2.IntegrityError as IE:
            self.cur.execute(
                "SELECT id FROM states WHERE status_name = %(status_name)s", state_info
            )

        out = self.cur.fetchone()[0]

        return out

    @eof_handler
    def delete_state(self, state_info):

        sql = """
                DELETE FROM states WHERE id=%(id)s
        """

        self.cur.execute(sql, state_info)

    @getter(
        "id house_number road suburb city state postcode country country_code",
        return_single=True,
    )
    @eof_handler
    def get_address(self, address_info):

        address_info = self._validate(address_info, self.database_features["address"])

        sql = """
            SELECT id, house_number, road, suburb, city, state, postcode, country, country_code
            FROM addresses
            WHERE (id=%(id)s OR %(id)s IS NULL) AND 
            (house_number=%(house_number)s OR %(house_number)s IS NULL) AND
            (road=%(road)s OR %(road)s IS NULL) AND
            (suburb=%(suburb)s OR %(suburb)s IS NULL) AND
            (city=%(city)s OR %(city)s IS NULL) AND
            (postcode=%(postcode)s OR %(postcode)s IS NULL) AND
            (country=%(country)s OR %(country)s IS NULL) AND
            (country_code=%(country_code)s OR %(country_code)s IS NULL)
        """

        self.cur.execute(sql, address_info)

        return self.cur.fetchall()

    @getter("id feature feature_rate")
    @eof_handler
    def get_features_list(self, feature_name):

        feature_name = feature_name + "%"

        sql = """
            SELECT id, feature, feature_rate FROM features_list WHERE feature LIKE %s
        """

        self.cur.execute(sql, [feature_name])

        return self.cur.fetchall()

    @eof_handler
    def features_delete(self, features_delete_list):

        for element in features_delete_list:
            sql = """
                DELETE FROM features_list WHERE feature=%s
            """

            self.cur.execute(sql, [element])

    @eof_handler
    def feature_update(self, update_list):

        sql = """
            UPDATE features_list SET feature = %s, feature_rate = %s WHERE id = %s
        """

        self.cur.execute(
            sql, [update_list["Feature"], update_list["Rate"], update_list["ID"]]
        )

    @eof_handler
    def feature_add(self, add_feature):
        sql = """
                INSERT INTO features_list VALUES(DEFAULT, %s, %s)
        """
        self.cur.execute(sql, [add_feature["Feature"], add_feature["Rate"]])

    @getter("id discount discount_rate")
    @eof_handler
    def get_discounts_list(self, discount_name):

        discount_name = discount_name + "%"

        sql = """
            SELECT id, discount_name, discount_rate FROM discounts_list WHERE discount_name LIKE %s
        """

        self.cur.execute(sql, [discount_name])

        return self.cur.fetchall()

    @eof_handler
    def discounts_delete(self, discounts_delete_list):

        for element in discounts_delete_list:
            sql = """
                DELETE FROM discounts_list WHERE discount_name=%s
            """

            self.cur.execute(sql, [element])

    @eof_handler
    def discount_update(self, update_list):

        sql = """
            UPDATE discounts_list SET discount_name = %s, discount_rate = %s WHERE id = %s
        """

        self.cur.execute(
            sql, [update_list["Discount"], update_list["Rate"], update_list["ID"]]
        )

    @eof_handler
    def discount_add(self, add_discount):
        sql = """
            INSERT INTO discounts_list VALUES(DEFAULT, %s, %s)
        """

        self.cur.execute(sql, [add_discount["Discount"], add_discount["Rate"]])

    @getter("station_id order_status step")
    @eof_handler
    def get_delivery_history(self, order_info):

        sql = """
            SELECT station_id, order_status, step
            FROM delivery_history WHERE order_id=%(order_id)s
            ORDER BY step ASC
        """

        self.cur.execute(sql, order_info)

        return self.cur.fetchall()

    # @eof_handler
    def add_address(self, address_info):

        address_info = self._validate(address_info, self.database_features['address'])
        sql = """  
            
            INSERT INTO addresses (house_number, road, suburb, city, state, postcode, country, country_code)
            VALUES (%(house_number)s, %(road)s, %(suburb)s, %(city)s, %(state)s, %(postcode)s, %(country)s, %(country_code)s);
            
            UPDATE orders SET delivery_address=currval('addresses_id_seq') WHERE id = %(order_id)s;

        """

        try:
            self.cur.execute(sql, address_info)
            

        except psycopg2.IntegrityError as IE:
            existing_address = self.get_address(address_info)

            if isinstance(existing_address, list):
               existing_address_id = existing_address[0]["id"]
            elif isinstance(existing_address, dict):
                existing_address_id = existing_address["id"]
            
            
            self.update_order(
                {
                    "id": address_info["order_id"],
                    "delivery_address": existing_address_id,
                }
            )

    @eof_handler
    def add_delivery_history(self, order_info):

        sql = """
            INSERT INTO delivery_history (order_id, station_id, order_status, step)
            VALUES (%(id)s, %(current_station)s, %(order_status)s, CURRENT_TIMESTAMP(2))
        """

        self.cur.execute(sql, order_info)

    @getter(
        "order_id first_name last_name phone_number company email road house_number "
        + "city status_name shipping_date delivery_date station_name station_address"
    )
    @eof_handler
    def get_order_displayinfo(self, order_id: str) -> tuple():
        sql = """
        SELECT orders.order_id, clients.first_name, clients.last_name, clients.phone_number, clients.company, 
            clients.email, addresses.road, addresses.house_number, addresses.city, states.status_name, orders.shipping_date, 
            orders.delivery_date, stations.station_name, stations.station_address FROM orders 
            LEFT JOIN clients ON orders.sender=clients.id
            LEFT JOIN stations ON orders.current_station=stations.id
            LEFT JOIN states ON orders.order_status=states.id
            LEFT JOIN addresses ON orders.delivery_address=addresses.id

            WHERE orders.order_id = %(order_id)s
        """
        self.cur.execute(sql, {"order_id": order_id})

        output = self.cur.fetchall()

        return output

    @getter(
        "order_id first_name last_name phone_number company email road house_number "
        + "city status_name shipping_date delivery_date station_name station_address"
    )
    @eof_handler
    def search_order(self, order_info):

        self._validate(
            order_info,
            (
                "order_id first_name last_name phone_number company email road house_number "
                + "city status_name shipping_date delivery_date station_name station_address"
            ).split(" "),
        )

        for element in order_info:
            if element not in ["shipping_date", "delivery_date"]:
                if order_info[element] is not None:
                    order_info[element] = order_info[element] + "%"

        sql = """
            SELECT orders.order_id, clients.first_name, clients.last_name, clients.phone_number, clients.company, 
            clients.email, addresses.road, addresses.house_number, addresses.city, states.status_name, orders.shipping_date, 
            orders.delivery_date, stations.station_name, stations.station_address FROM orders 
            LEFT JOIN clients ON orders.sender=clients.id
            LEFT JOIN stations ON orders.current_station=stations.id
            LEFT JOIN states ON orders.order_status=states.id
            LEFT JOIN addresses ON orders.delivery_address=addresses.id
            WHERE (orders.order_id LIKE %(order_id)s OR %(order_id)s IS NULL) AND
            (clients.first_name LIKE %(first_name)s OR %(first_name)s IS NULL) AND
            (clients.last_name LIKE %(last_name)s OR %(last_name)s IS NULL) AND
            (clients.phone_number LIKE %(phone_number)s OR %(phone_number)s IS NULL) AND
            (clients.company LIKE %(company)s OR %(company)s IS NULL) AND
            (clients.email LIKE %(email)s OR %(email)s IS NULL) AND
            (addresses.city LIKE %(city)s OR %(city)s IS NULL) AND
            (states.status_name LIKE %(status_name)s OR %(status_name)s IS NULL) AND
            (orders.shipping_date >= CAST(%(shipping_date)s AS DATE)) AND
            (orders.delivery_date >= CAST(%(delivery_date)s AS DATE)) AND
            (stations.station_name LIKE %(station_name)s OR %(station_name)s IS NULL)
        """

        self.cur.execute(sql, order_info)
        return self.cur.fetchall()
