import logging
from random import randint
from time import sleep

from password_utils.password_handler import EmailSender, PasswordGenerator

from .database_base import DatabaseManager
from .database_decorators import eof_handler, getter

logger = logging.getLogger("app")


class UserDatabaseManager(DatabaseManager):
    def __init__(self, dbname, user, password, host, port):
        super().__init__(dbname, user, password, host, port)
        self.email_sender = EmailSender()
        self.password_generator = PasswordGenerator()

    @eof_handler
    def add_user(self, user_info):
        password = self.password_generator.generate_password()
        position_id = self.get_position(user_info)["id"]

        sql = """
            INSERT INTO users(first_name, last_name, email, password_hash, position_id)
            VALUES (%s, %s, %s, %s, %s)
        """

        self.cur.execute(
            sql,
            [
                user_info["first_name"],
                user_info["last_name"],
                user_info["email"],
                password,
                position_id,
            ],
        )

        code = self.password_generator.generate_link()

        self.add_password_code(self.get_user(user_info)[0], code)

        self.email_sender.send_email(user_info["email"], code)

    @eof_handler
    def update_user(self, user_info):

        user_info["id"] = int(user_info["id"])

        user_info["position_id"] = self.get_position(user_info)["id"]

        sql = """
            UPDATE users SET first_name=%s, last_name=%s, email=%s, password_hash=%s, position_id=%s
            WHERE id=%s
        """

        self.cur.execute(
            sql,
            [
                user_info["first_name"],
                user_info["last_name"],
                user_info["email"],
                user_info["password"],
                user_info["position_id"],
                user_info["id"],
            ],
        )

    @eof_handler
    def delete_user(self, user_info):
        user_info["id"] = int(user_info["id"])

        sql = """
            DELETE FROM reset_password WHERE user_id=%s;
            DELETE FROM users WHERE first_name=%s AND last_name=%s AND email=%s AND id=%s
        """

        self.cur.execute(
            sql,
            [
                user_info["id"],
                user_info["first_name"],
                user_info["last_name"],
                user_info["email"],
                user_info["id"],
            ],
        )

    @eof_handler
    def update_user_position(self, position_info):

        position_id = self.get_position(position_info)

        sql = """
            UPDATE users SET position_id = %s 
        """
        self.cur.execute(sql, [position_id])

    @getter("name")
    @eof_handler
    def get_uniq_permissions(self):
        sql = """
            SELECT DISTINCT permission_name from permissions
        """
        self.cur.execute(sql)

        return self.cur.fetchall()

    @getter("name")
    @eof_handler
    def get_user_permissions(self, user_id):

        position_id = self.get_user_position(user_id)

        sql = """
                    SELECT permission_name FROM permissions
                    WHERE position_id=%s
                """

        self.cur.execute(
            sql,
            [
                position_id["id"],
            ],
        )

        return self.cur.fetchall()

    @eof_handler
    def update_position_permission(self, position_info):

        args = [
            position_info["position_name"],
            position_info["id"],
            position_info["id"],
            position_info["id"],
        ]
        sql = """
            UPDATE positions SET position_name=%s where id=%s;
            DELETE FROM permissions where position_id=%s;
            DELETE FROM permissions where position_id=%s;
        """

        for element in position_info["permissions"]:
            args.extend([position_info["id"], element])
            sql += """
                    INSERT INTO permissions (position_id, permission_name)
                    VALUES (%s, %s);
            """

        self.cur.execute(sql, args)

    @eof_handler
    def delete_position_permission(self, position_info):

        sql = """
            UPDATE users SET position_id=(SELECT COALESCE((SELECT id FROM positions WHERE position_name='SUSPENDED'), 0)) where position_id=%(id)s;
            DELETE FROM permissions WHERE position_id=%(id)s;
            DELETE FROM positions WHERE id=%(id)s AND position_name=%(position_name)s;
        """

        self.cur.execute(sql, position_info)

    @getter("id position_name permission_name")
    @eof_handler
    def get_positions_with_permissions(self, position_info):

        position_info["position_name"] += "%"

        sql = """
            SELECT positions.id, positions.position_name, permissions.permission_name 
            FROM permissions 
            RIGHT JOIN positions ON permissions.position_id = positions.id
            WHERE positions.position_name LIKE %(position_name)s
        """
        self.cur.execute(sql, position_info)

        return self.cur.fetchall()

    @getter("id", return_single=True)
    @eof_handler
    def get_user_position(self, user_id):
        sql = """
            SELECT position_id FROM users WHERE id=%s
        """

        self.cur.execute(sql, [user_id])

        return self.cur.fetchall()

    @getter("id", return_single=True)
    @eof_handler
    def get_position(self, position_info):
        logger.info(f"get_position_id input: {position_info}")
        sql = """
                   SELECT id FROM positions WHERE position_name=%s
        """
        self.cur.execute(sql, [position_info["position_name"]])

        rv = self.cur.fetchall()
        logger.info(f"get_position_id output: {rv}")
        return rv

    @eof_handler
    def add_position(self, position_info):

        args = [position_info["position_name"]]
        sql = """
                INSERT INTO positions (position_name) VALUES (%s);
        """

        for element in position_info["permissions"]:
            args.append(element)
            sql += """
                    INSERT INTO permissions (position_id, permission_name)
                    VALUES (currval('positions_id_seq'), %s);
            """

        self.cur.execute(sql, args)

    @getter("id first_name last_name email position_name")
    @eof_handler
    def get_user(self, user_info):
        required_keys = ["first_name", "last_name", "email", "position_name"]

        for key in required_keys:
            if key not in user_info:
                user_info[key] = None

        logger.info(f"get_user: {user_info}")

        sql = """
            SELECT users.id, users.first_name, users.last_name, users.email, positions.position_name FROM users 
            LEFT JOIN positions ON users.position_id=positions.id WHERE
            (users.first_name=%(first_name)s OR %(first_name)s IS NULL) AND 
            (users.last_name=%(last_name)s OR %(last_name)s IS NULL) AND
            (users.email=%(email)s OR %(email)s IS NULL) AND
            (positions.position_name=%(position_name)s OR %(position_name)s IS NULL)
        """

        self.cur.execute(sql, user_info)

        rv = self.cur.fetchall()
        logger.info(f"{rv}")
        return rv

    @getter("id", return_single=True)
    @eof_handler
    def get_user_login(self, user_info):
        sql = """
            SELECT id FROM users 
            WHERE 
            (email=%(email)s) AND
            (password_hash=crypt(%(password)s, password_hash))
        """
        self.cur.execute(sql, user_info)

        return self.cur.fetchall()

    @getter("case", return_single=True)
    @eof_handler
    def reset_password_check(self, code):
        sql = """
            SELECT CASE WHEN EXISTS (
                SELECT * FROM reset_password
                WHERE code = %s
            )
            THEN CAST (1 AS BIT)
            ELSE CAST (0 AS BIT)
            END
        """
        self.cur.execute(sql, [code])

        return self.cur.fetchall()

    @eof_handler
    def reset_password(self, password, code):
        args = [password, code, code]
        sql = """
            UPDATE users SET password_hash=%s
            WHERE id=(SELECT user_id FROM reset_password WHERE code=%s);
            DELETE FROM reset_password WHERE code=%s;
        """

        self.cur.execute(sql, args)

    @eof_handler
    def add_password_code(self, user_info, code):
        sql = """
            INSERT INTO reset_password (user_id, code)
            VALUES ((SELECT id from users where email=%s), %s)
        """
        self.cur.execute(sql, [user_info["email"], code])

    @eof_handler
    def send_reset_link(self, email):
        user_info = {"email": email}
        if len(self.get_user(user_info)) != 1:
            sleep(randint(4, 5))
            return
        code = self.password_generator.generate_link()
        self.add_password_code(user_info, code)
        self.email_sender.send_password_reset(user_info["email"], code)


def permissions_mapping(permissions):

    print(permissions)

    mapping_pattern = {
        "add_user": {"route": "/add_user", "display_name": "Add User"},
        "delete_user": {"route": "/delete_user", "display_name": "Delete User"},
        "update_user": {"route": "/update_user", "display_name": "Update User"},
        "add_position": {"route": "/add_position", "display_name": "Add Postion"},
        "update_position": {
            "route": "/update_position",
            "display_name": "Update position",
        },
        "delete_position": {
            "route": "/delete_position",
            "display_name": "Delete Position",
        },
        "update_client": {"route": "/update_client", "display_name": "Update Client"},
        "delete_client": {"route": "/delete_client", "display_name": "Delete Client"},
        "manage_features": {
            "route": "/manage_features",
            "display_name": "Manage features",
        },
        "manage_discounts": {
            "route": "/manage_discounts",
            "display_name": "Manage discounts",
        },
        "search_order": {"route": "/search_order", "display_name": "Search order"},
        "add_order": {"route": "/add_order/sender", "display_name": "Add Order"},
    }

    reformatted_permissions = {}
    for permission in permissions:
        if permission in mapping_pattern:
            reformatted_permissions[permission] = mapping_pattern[permission]

    return reformatted_permissions
