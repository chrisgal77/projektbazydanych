from functools import wraps

import pydantic
from flask import flash
from psycopg2 import InterfaceError, InternalError, OperationalError, errorcodes


def getter(return_keys, return_single=False):
    return_keys = return_keys.split(" ")

    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            rv = function(*args, **kwargs)
            result = []
            try:
                for row in rv:
                    data = {}
                    for name, column in zip(return_keys, row):
                        data[name] = column
                    result.append(data)
                if return_single and len(result) == 1:
                    return result[0]
                if return_single and not result:
                    return {key: None for key in return_keys}
                return result
            except Exception as e:
                return None

        return wrapper

    return decorator


def eof_handler(function):
    def decorator(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except (OperationalError, InterfaceError) as e:
            if args[0].connected() != 0:
                args[0].connect()
            try:
                return function(*args, **kwargs)
            except InternalError as e:
                if e.pgcode == "P0001":
                    flash(e.pgerror[0 : (e.pgerror.find("CONTEXT:"))], "danger")
                else:
                    flash(errorcodes.lookup(e.pgcode), "danger")
            except Exception as e:
                flash(errorcodes.lookup(e.pgcode), "danger")
        except InternalError as e:
            if e.pgcode == "P0001":
                flash(e.pgerror[0 : (e.pgerror.find("CONTEXT:"))], "danger")
            else:
                flash(errorcodes.lookup(e.pgcode), "danger")
        except Exception as e:
            flash(errorcodes.lookup(e.pgcode), "danger")

    return decorator
