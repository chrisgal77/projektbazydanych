CREATE TRIGGER trigger_hash_pass BEFORE INSERT ON users FOR EACH ROW EXECUTE PROCEDURE func_hash_pass();

CREATE OR REPLACE FUNCTION func_hash_pass() RETURNS TRIGGER AS $$
BEGIN
  new.password_hash = crypt(new.password_hash, gen_salt('bf', 5));
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER trigger_update_user BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE func_update_user();

CREATE OR REPLACE FUNCTION func_update_user() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.first_name IS NULL THEN
  	NEW.first_name = OLD.first_name;
  END IF;
  IF NEW.last_name IS NULL THEN
  	NEW.last_name = OLD.last_name;
  END IF;
  IF NEW.email IS NULL THEN
  	NEW.email = OLD.email;
  END IF;
  IF NEW.password_hash IS NULL THEN
  	NEW.password_hash = OLD.password_hash;
  ELSIF new.password_hash IS NOT NULL AND new.password_hash != (SELECT password_hash FROM users where id=new.id) THEN
        new.password_hash = crypt(new.password_hash, gen_salt('bf', 5));
  END IF;
  IF NEW.position_id IS NULL THEN
  	NEW.position_id = OLD.position_id;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER trigger_delete_old_codes BEFORE INSERT ON reset_password FOR EACH ROW EXECUTE PROCEDURE delete_old_codes();

CREATE OR REPLACE FUNCTION delete_old_codes() RETURNS TRIGGER AS $$
BEGIN
   IF (SELECT exists (SELECT 1 FROM reset_password WHERE user_id = NEW.user_id LIMIT 1)) = TRUE THEN
	DELETE FROM reset_password WHERE user_id=NEW.user_id;
   END IF;
   RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Vacuum every day at 03:00
SELECT cron.schedule('0 3 * * *', $$DELETE FROM reset_password WHERE code_time < CURRENT_TIMESTAMP - INTERVAL '23 hours'$$);

CREATE OR REPLACE FUNCTION calc_cost(X int, Y int, Z int, package_weight NUMERIC, feat_list text[], disc_list text[]) RETURNS NUMERIC AS $$
DECLARE
   calc_capacity INT := ABS(X*Y*Z);
   get_cap_rate INT := (SELECT COALESCE((SELECT capacity_rate FROM capacity_multipliers WHERE capacity >= calc_capacity ORDER BY ABS(capacity - calc_capacity) ASC LIMIT 1),4000));
   calc_weight NUMERIC(12,2) := calc_capacity::NUMERIC(12,2)/get_cap_rate::NUMERIC(12,2);
   calc_cost NUMERIC(12,2) := 1;
   rate NUMERIC;
   feat_name TEXT;
   disc_name TEXT;
BEGIN
   IF X < 0 OR Y < 0 OR Z < 0 OR package_weight < 0 THEN
    RAISE EXCEPTION 'Wrong numbers or wrong types' USING HINT = 'Check numbers';
    RETURN calc_cost;
   END IF;
   IF calc_weight >= package_weight THEN
     IF (SELECT weight_rate FROM weight_multipliers WHERE weight >= calc_weight ORDER BY ABS(weight - calc_weight) ASC LIMIT 1) IS NOT NULL THEN
       calc_cost := calc_weight + calc_weight * SUM((SELECT weight_rate FROM weight_multipliers WHERE weight >= calc_weight ORDER BY ABS(weight - calc_weight) ASC LIMIT 1)/100);
     ELSE
        calc_cost := calc_weight + calc_weight * SUM((SELECT MAX(weight_rate) FROM weight_multipliers)/100);
     END IF;
   ELSE
     IF (SELECT weight_rate FROM weight_multipliers WHERE weight >= package_weight ORDER BY ABS(weight - package_weight) ASC LIMIT 1) IS NOT NULL THEN
        calc_cost := package_weight + package_weight * SUM((SELECT weight_rate FROM weight_multipliers WHERE weight >= package_weight ORDER BY ABS(weight - package_weight) ASC LIMIT 1)/100);
     ELSE
        calc_cost := package_weight + package_weight * SUM((SELECT MAX(weight_rate) FROM weight_multipliers)/100);
     END IF;
   END IF;
   
   FOREACH feat_name in ARRAY feat_list
   LOOP
   continue WHEN (SELECT feature_rate FROM features_list WHERE feature = feat_name) is NULL;
   calc_cost := calc_cost + calc_cost * SUM((SELECT feature_rate FROM features_list WHERE feature = feat_name)/100);
   END LOOP;

   FOREACH disc_name in ARRAY disc_list
   LOOP
   continue WHEN (SELECT discount_rate FROM discounts_list WHERE discount_name = disc_name) is NULL;
   calc_cost := calc_cost - calc_cost * SUM((SELECT discount_rate FROM discounts_list WHERE discount_name = disc_name)/100);
   END LOOP;
    
   RETURN calc_cost;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_features BEFORE UPDATE ON features_list FOR EACH ROW EXECUTE PROCEDURE func_update_features();

CREATE OR REPLACE FUNCTION func_update_features() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.feature IS NULL THEN
  	NEW.feature := OLD.feature;
  END IF;
  IF NEW.feature_rate IS NULL THEN
  	NEW.feature_rate := OLD.feature_rate;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_discounts BEFORE UPDATE ON discounts_list FOR EACH ROW EXECUTE PROCEDURE func_update_discounts();

CREATE OR REPLACE FUNCTION func_update_discounts() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.discount_name IS NULL THEN
  	NEW.discount_name = OLD.discount_name;
  END IF;
  IF NEW.discount_rate IS NULL THEN
  	NEW.discount_rate = OLD.discount_rate;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_date BEFORE UPDATE ON orders FOR EACH ROW EXECUTE PROCEDURE func_update_date();

CREATE OR REPLACE FUNCTION func_update_date() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.delivery_date < OLD.shipping_date OR NEW.delivery_date <= CURRENT_DATE THEN
    RAISE EXCEPTION 'Wrong delivery date: %', NEW.delivery_date USING HINT = 'Check delivery date';
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;