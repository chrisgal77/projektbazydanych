CREATE TABLE IF NOT EXISTS addresses (
    id SERIAL,
    house_number TEXT,
    road TEXT,
    suburb TEXT,
    city TEXT NOT NULL,
    state TEXT,
    postcode TEXT,
    country NOT NULL TEXT,
    country_code TEXT
    PRIMARY KEY (id)
    CONSTRAINT unique_add UNIQUE (house_number, road, city, country)
);

CREATE TABLE IF NOT EXISTS states (
    id SERIAL, 
    status_name TEXT NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS clients (
    id SERIAL,
    email TEXT NOT NULL,
    phone_number TEXT,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    company TEXT,
    PRIMARY KEY (id)
    CONSTRAINT unique_all UNIQUE (email, phone_number, first_name, last_name, company)
);

CREATE TABLE IF NOT EXISTS stations (
    id SERIAL,
    station_name TEXT NOT NULL,
    station_address INT NOT NULL UNIQUE,
    PRIMARY KEY (id),
    FOREIGN KEY (station_address) REFERENCES addresses(id)
);

CREATE SEQUENCE orders_id_seq;

CREATE TABLE IF NOT EXISTS orders (
    id INT DEFAULT nextval('orders_id_seq') NOT NULL UNIQUE,
    order_id text DEFAULT md5(currval('orders_id_seq')::text) NOT NULL UNIQUE,
    delivery_address INT NOT NULL,
    shipping_address INT NOT NULL,
    order_status INT NOT NULL,
    sender INT NOT NULL,
    recipient INT NOT NULL,
    shipping_date TIMESTAMP NOT NULL,
    delivery_date TIMESTAMP,
    current_station INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (current_station) REFERENCES stations(id),
    FOREIGN KEY (sender) REFERENCES clients(id),
    FOREIGN KEY (recipient) REFERENCES clients(id),
    FOREIGN KEY (delivery_address) REFERENCES addresses(id),
    FOREIGN KEY (shipping_address) REFERENCES addresses(id),
    FOREIGN KEY (order_status) REFERENCES states(id)
);

CREATE TABLE IF NOT EXISTS dimensions (
    id SERIAL,
    X INT NOT NULL,
    Y INT NOT NULL,
    Z INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS packages (
    id SERIAL,
    order_id TEXT NOT NULL,
    dimensions_id INT NOT NULL,
    package_weight NUMERIC NOT NULL,
    cost NUMERIC(12,2) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
	FOREIGN KEY (dimensions_id) REFERENCES dimensions(id)
);

CREATE TABLE IF NOT EXISTS features_list (
	id SERIAL,
	feature TEXT NOT NULL UNIQUE,
	feature_rate NUMERIC(8, 4) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS features (
  id SERIAL,
  package_id INT NOT NULL,
  feature_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (package_id) REFERENCES packages(id),
  FOREIGN KEY (feature_id) REFERENCES features_list(id)
);

CREATE TABLE IF NOT EXISTS capacity_multipliers (
	id SERIAL,
	capacity INT NOT NULL UNIQUE,
	capacity_rate INT NOT NULL UNIQUE,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS weight_multipliers (
	id SERIAL,
	weight INT NOT NULL UNIQUE,
	weight_rate NUMERIC(8, 4) NOT NULL UNIQUE,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS discounts_list (
	id SERIAL,
	discount_name TEXT NOT NULL UNIQUE,
	discount_rate NUMERIC(8, 4) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS discounts (
	id SERIAL,
	package_id INT NOT NULL,
	discount_id INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (package_id) REFERENCES packages(id),
	FOREIGN KEY (discount_id) REFERENCES discounts_list(id)
);

CREATE TABLE IF NOT EXISTS delivery_history (
  id SERIAL,
  station_id INT NOT NULL,
  order_id INT NOT NULL,
  order_status INT NOT NULL,
  step TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (station_id) REFERENCES stations(id),
	FOREIGN KEY (order_id) REFERENCES orders(id)
)

CREATE OR REPLACE FUNCTION calc_cost(X int, Y int, Z int, package_weight NUMERIC, feat_list text[], disc_list text[]) RETURNS NUMERIC AS $$
DECLARE
   calc_capacity INT := ABS(X*Y*Z);
   get_cap_rate INT := (SELECT COALESCE((SELECT capacity_rate FROM capacity_multipliers WHERE capacity >= calc_capacity ORDER BY ABS(capacity - calc_capacity) ASC LIMIT 1),4000));
   calc_weight NUMERIC(12,2) := calc_capacity::NUMERIC(12,2)/get_cap_rate::NUMERIC(12,2);
   calc_cost NUMERIC(12,2) := 1;
   rate NUMERIC;
   feat_name TEXT;
   disc_name TEXT;
BEGIN
   IF X < 0 OR Y < 0 OR Z < 0 OR package_weight < 0 THEN
    RAISE EXCEPTION 'Wrong numbers or wrong types' USING HINT = 'Check numbers';
    RETURN calc_cost;
   END IF;
   IF calc_weight >= package_weight THEN
     IF (SELECT weight_rate FROM weight_multipliers WHERE weight >= calc_weight ORDER BY ABS(weight - calc_weight) ASC LIMIT 1) IS NOT NULL THEN
       calc_cost := calc_weight + calc_weight * SUM((SELECT weight_rate FROM weight_multipliers WHERE weight >= calc_weight ORDER BY ABS(weight - calc_weight) ASC LIMIT 1)/100);
     ELSE
        calc_cost := calc_weight + calc_weight * SUM((SELECT MAX(weight_rate) FROM weight_multipliers)/100);
     END IF;
   ELSE
     IF (SELECT weight_rate FROM weight_multipliers WHERE weight >= package_weight ORDER BY ABS(weight - package_weight) ASC LIMIT 1) IS NOT NULL THEN
        calc_cost := package_weight + package_weight * SUM((SELECT weight_rate FROM weight_multipliers WHERE weight >= package_weight ORDER BY ABS(weight - package_weight) ASC LIMIT 1)/100);
     ELSE
        calc_cost := package_weight + package_weight * SUM((SELECT MAX(weight_rate) FROM weight_multipliers)/100);
     END IF;
   END IF;
   
   FOREACH feat_name in ARRAY feat_list
   LOOP
   continue WHEN (SELECT feature_rate FROM features_list WHERE feature = feat_name) is NULL;
   calc_cost := calc_cost + calc_cost * SUM((SELECT feature_rate FROM features_list WHERE feature = feat_name)/100);
   END LOOP;

   FOREACH disc_name in ARRAY disc_list
   LOOP
   continue WHEN (SELECT discount_rate FROM discounts_list WHERE discount_name = disc_name) is NULL;
   calc_cost := calc_cost - calc_cost * SUM((SELECT discount_rate FROM discounts_list WHERE discount_name = disc_name)/100);
   END LOOP;
    
   RETURN calc_cost;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION func_update_features() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.feature IS NULL THEN
  	NEW.feature := OLD.feature;
  END IF;
  IF NEW.feature_rate IS NULL THEN
  	NEW.feature_rate := OLD.feature_rate;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_features BEFORE UPDATE ON features_list FOR EACH ROW EXECUTE PROCEDURE func_update_features();


CREATE OR REPLACE FUNCTION func_update_discounts() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.discount_name IS NULL THEN
  	NEW.discount_name = OLD.discount_name;
  END IF;
  IF NEW.discount_rate IS NULL THEN
  	NEW.discount_rate = OLD.discount_rate;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_discounts BEFORE UPDATE ON discounts_list FOR EACH ROW EXECUTE PROCEDURE func_update_discounts();

CREATE OR REPLACE FUNCTION func_update_date() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.delivery_date < OLD.shipping_date OR NEW.delivery_date <= CURRENT_DATE THEN
    RAISE EXCEPTION 'Wrong delivery date: %', NEW.delivery_date USING HINT = 'Check delivery date';
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_date BEFORE UPDATE ON orders FOR EACH ROW EXECUTE PROCEDURE func_update_date();
